library(data.table)

command_awk <- "awk '/^\\*\\/$/{a=1} a'"

source(here::here("scripts", "R","column_rename.R"))
source(here::here("scripts", "R","column_convert.R"))

output <- list()

# Importing datasets
## Sequencing

unzip(here::here("data","TARA_SAMPLES_CONTEXT_SEQUENCING_20170515.zip"),exdir=here::here("tmp"))

variables <- readxl::read_excel(here::here("tmp","TARA_SAMPLES_CONTEXT_SEQUENCING_20170515.xlsx"), range="B17:Y20", col_names = FALSE) |>
  t() |>
  data.table()

setnames(variables,c("parameter","pi","method","comment"))

values <- readxl::read_excel(here::here("tmp","TARA_SAMPLES_CONTEXT_SEQUENCING_20170515.xlsx"),range="B22:Y3413",col_names = FALSE,col_types="text") |>
  data.table()

new_colnames <- c(
  "sample_id_pangaea",
  "sample_id_biosamples",
  "sample_id_ena",
  "station_label",
  "event_label",
  "env_feature",
  "depth_nominal",
  "depth_top",
  "depth_bottom",
  "size_fraction_lower_threshold",
  "size_fraction_upper_threshold",
  "sample_material",
  "sample_method",
  "sample_code",
  "study_accession",
  "nucleic_acids_preparation",
  "amplicon_generation",
  "illumina_libraries_preparation",
  "library_name",
  "experiment_accession",
  "run_accession",
  "run_alias",
  "nucleotides_data_file_fastq",
  "nucleotides_data_file"
)

variables[,name:=new_colnames]
setnames(values,new_colnames)

output[["sequencing"]][["values"]] <- values
output[["sequencing"]][["variables"]] <- variables[,list(name,parameter,pi,method,comment)]

## WATERCOLUMN
values <- fread(cmd=paste(command_awk,here::here("data","TARA_SAMPLES_CONTEXT_ENV-WATERCOLUMN.tab")))
variables <- fread(cmd=paste("awk -f",here::here("scripts", "awk","parse_variable.awk"),here::here("data","TARA_SAMPLES_CONTEXT_ENV-WATERCOLUMN.tab")))

new_colnames <- c(
  "sample_id_pangaea",
  "sample_id_biosamples",
  "sample_id_ena",
  "event_basis",
  "event_compaign",
  "station_label",
  "event_method",
  "event_label",
  "event_time",
  "event_latitude",
  "event_longitude",
  "env_feature",
  "depth_nominal",
  "depth_top",
  "depth_bottom",
  "size_fraction_lower_threshold",
  "size_fraction_upper_threshold",
  "sample_material",
  "sample_method",
  "sample_code",
  "marine_biome",
  "ocean_region",
  "biogeo_province",
  "sea_ice_conc",
  "sea_ice_free_period_start",
  "sea_ice_free_period_duration",
  "sea_ice_free_period_end",
  "season",
  "season_part",
  "moon_phase_nominal",
  "moon_phase_proportion",
  "time_of_day",
  "sunshine_duration",
  "par_day_amodis",
  "par_day_amodis_8days_avg",
  "par_day_gsm_8days_avg",
  "par_day_amodis_30days_avg",
  "sst_grad_h_amodis_8days_avg",
  "iron_darwin_5m",
  "iron_darwin_5m_std",
  "ammonium_darwin_5m",
  "ammonium_darwin_5m_std",
  "nitrite_darwin_5m",
  "nitrite_darwin_5m_std",
  "nitrate_darwin_5m",
  "nitrate_darwin_5m_std",
  "ssfcdom_amodis",
  "ssfchla_gsm",
  "chla",
  "ssphi_amodis",
  "npp_c_uqar_takuvik",
  "npp_c_vgpm",
  "tss_meris",
  "poc_gsm",
  "pic_gsm",
  "depth_bathy",
  "closest_coast_latitude",
  "closest_coast_longitude",
  "closest_coast_distance",
  "sst_grad_h",
  "ssm_index",
  "geostrophic_velocity_longitudinal",
  "geostrophic_velocity_latitudinal",
  "okubo_weiss",
  "maximum_lyapunov_exponent",
  "residence_time",
  "origin_500m_latitude",
  "origin_500m_longitude",
  "origin_500m_age",
  "origin_45days_latitude",
  "origin_45days_longitude"
)

variables[,name:=new_colnames]
setnames(values,new_colnames)

output[["watercolumn"]][["values"]] <- values
output[["watercolumn"]][["variables"]]  <- variables[,list(name,parameter,pi,method,comment)]

## CARB
unzip(here::here("data","TARA_SAMPLES_CONTEXT_ENV-DEPTH-CARB_20170515.zip"),exdir=here::here("tmp"))

variables <- readxl::read_excel(here::here("tmp","TARA_SAMPLES_CONTEXT_ENV-DEPTH-CARB_20170515.xlsx"), range="B18:BQ21", col_names = FALSE) |>
  t() |>
  data.table()

setnames(variables,c("parameter","pi","method","comment"))

values <- readxl::read_excel(here::here("tmp","TARA_SAMPLES_CONTEXT_ENV-DEPTH-CARB_20170515.xlsx"),range="B23:BQ34798",col_names = FALSE,col_types="text") |>
  data.table()

new_colnames <- c(
  "sample_id_pangaea",
  "sample_id_biosamples",
  "sample_id_ena",
  "station_label",
  "event_label",
  "env_feature",
  "depth_nominal",
  "depth_top",
  "depth_bottom",
  "size_fraction_lower_threshold",
  "size_fraction_upper_threshold",
  "sample_material",
  "sample_method",
  "sample_code",
  "file_name",
  "distance_to_sample",
  "duration",
  "n_observations",
  add_quant("ph"),
  add_quant("carbon_dioxide"),
  add_quant("carbon_dioxide_pp"),
  add_quant("carbon_dioxide_fugacity"),
  add_quant("bicarbonate_ion"),
  add_quant("carbonate_ion"),
  add_quant("total_carbon"),
  add_quant("alkalinity"),
  add_quant("calcite_saturation"),
  add_quant("aragonite_saturation")
)

variables[,name:=new_colnames]
setnames(values,new_colnames)

output[["carb"]][["values"]] <- values
output[["carb"]][["variables"]]  <- variables[,list(name,parameter,pi,method,comment)]

## NUT
unzip(here::here("data","TARA_SAMPLES_CONTEXT_ENV-DEPTH-NUT_20170515.zip"),exdir=here::here("tmp"))

variables <- readxl::read_excel(here::here("tmp","TARA_SAMPLES_CONTEXT_ENV-DEPTH-NUT_20170515.xlsx"), range="B18:AM21", col_names = FALSE) |>
  t() |>
  data.table()

setnames(variables,c("parameter","pi","method","comment"))

values <- readxl::read_excel(here::here("tmp","TARA_SAMPLES_CONTEXT_ENV-DEPTH-NUT_20170515.xlsx"),range="B23:AM34798",col_names = FALSE,col_types="text") |>
  data.table()

new_colnames <- c(
  "sample_id_pangaea",
  "sample_id_biosamples",
  "sample_id_ena",
  "station_label",
  "event_label",
  "env_feature",
  "depth_nominal",
  "depth_top",
  "depth_bottom",
  "size_fraction_lower_threshold",
  "size_fraction_upper_threshold",
  "sample_material",
  "sample_method",
  "sample_code",
  "file_name",
  "distance_to_sample",
  "duration",
  "n_observations",
  add_quant("nitrite"),
  add_quant("phosphate"),
  add_quant("nitrate_nitrite"),
  add_quant("silicate")
)

variables[,name:=new_colnames]
setnames(values,new_colnames)

output[["nut"]][["values"]] <- values
output[["nut"]][["variables"]]  <- variables[,list(name,parameter,pi,method,comment)]

## SENSORS

values <- fread(cmd=paste(command_awk,here::here("data","TARA_SAMPLES_CONTEXT-DEPTH-SENSORS.tab")))
variables <- fread(cmd=paste("awk -f",here::here("scripts", "awk","parse_variable.awk"),here::here("data","TARA_SAMPLES_CONTEXT-DEPTH-SENSORS.tab")))

new_colnames <- c(
  "sample_id_pangaea",
  "sample_id_biosamples",
  "sample_id_ena",
  "event_basis",
  "event_compaign",
  "station_label",
  "event_method",
  "event_label",
  "event_time",
  "event_latitude",
  "event_longitude",
  "env_feature",
  "depth_nominal",
  "depth_top",
  "depth_bottom",
  "size_fraction_lower_threshold",
  "size_fraction_upper_threshold",
  "sample_material",
  "sample_method",
  "sample_code",
  "distance_to_sample",
  "duration",
  "file_name",
  "n_observations",
  add_quant("temperature"),
  add_quant("conductivity"),
  add_quant("salinity"),
  add_quant("tpot"),
  add_quant("sigma_theta"),
  add_quant("oxygen"),
  add_quant("oxygen_woa09_calib"),
  add_quant("nitrate"),
  add_quant("chla"),
  add_quant("chla_npq_cor"),
  add_quant("chla_npq_cor_calib"),
  add_quant("angular_scattering_coef_470"),
  add_quant("angular_scattering_coef_470_dark"),
  add_quant("angular_scattering_coef_470_cor_dark"),
  add_quant("optical_backscattering_coef_470_cor_dark"),
  add_quant("backscattering_coef_470_cor_dark"),
  add_quant("fcdom"),
  add_quant("optical_beam_attenuation_coef_660"),
  add_quant("beam_attenuation_coef_particles"),
  add_quant("par_day"),
  add_quant("par"),
  add_quant_short("kdpar_day_1day_avg"),
  add_quant_short("kdpar_1day_avg"),
  add_quant_short("kdpar_day_8day_avg"),
  add_quant_short("kdpar_8day_avg"),
  add_quant_short("kdpar_day_30day_avg"),
  add_quant_short("kdpar_30day_avg")
)

variables[,name:=new_colnames]
setnames(values,new_colnames)

output[["sensors"]][["values"]] <- values
output[["sensors"]][["variables"]]  <- variables[,list(name,parameter,pi,method,comment)]

## HPLC
unzip(here::here("data","TARA_SAMPLES_CONTEXT_ENV-DEPTH-HPLC_20170515.zip"),exdir=here::here("tmp"))

variables <- readxl::read_excel(here::here("tmp","TARA_SAMPLES_CONTEXT_ENV-DEPTH-HPLC_20170515.xlsx"), range="B18:EN21", col_names = FALSE) |>
  t() |>
  data.table()

setnames(variables,c("parameter","pi","method","comment"))

values <- readxl::read_excel(here::here("tmp","TARA_SAMPLES_CONTEXT_ENV-DEPTH-HPLC_20170515.xlsx"),range="B23:EN34798",col_names = FALSE,col_types="text") |>
  data.table()

new_colnames <- c(
  "sample_id_pangaea",
  "sample_id_biosamples",
  "sample_id_ena",
  "station_label",
  "event_label",
  "env_feature",
  "depth_nominal",
  "depth_top",
  "depth_bottom",
  "size_fraction_lower_threshold",
  "size_fraction_upper_threshold",
  "sample_material",
  "sample_method",
  "sample_code",
  "distance_to_sample",
  "duration",
  "file_name",
  "n_observations",
  add_quant("chlc3"),
  add_quant("chlc1_chlc2"),
  add_quant("chla_chla_like"),
  add_quant("peridinin"),
  add_quant("phaeophorbide_a"),
  add_quant("19_butanoyloxyfucoxanthin"),
  add_quant("fucoxanthin"),
  add_quant("neoxanthin"),
  add_quant("prasinoxanthin"),
  add_quant("violaxanthin"),
  add_quant("19_hexanoyloxyfucoxanthin"),
  add_quant("diadinoxanthin"),
  add_quant("alloxanthin"),
  add_quant("diatoxanthin"),
  add_quant("zeaxanthin"),
  add_quant("lutein"),
  add_quant("bacteriochlorophyll_a"),
  add_quant("divinyl_chlb"),
  add_quant("chlb"),
  add_quant("chlb_divinyl_chlb"),
  add_quant("divinyl_chla"),
  add_quant("chla"),
  add_quant("chla_total"),
  add_quant("phaeophytin_a"),
  add_quant("carotene")
)

variables[,name:=new_colnames]
setnames(values,new_colnames)

output[["hplc"]][["values"]] <- values
output[["hplc"]][["variables"]]  <- variables[,list(name,parameter,pi,method,comment)]

## Method

unzip(here::here("data","TARA_SAMPLES_CONTEXT_METHODS.zip"),exdir=here::here("tmp"))

variables <- readxl::read_excel(here::here("tmp","TARA_SAMPLES_CONTEXT_METHODS_20180320.xlsx"), range="B18:BA21", col_names = FALSE) |>
  t() |>
  data.table()

setnames(variables,c("parameter","pi","method","comment"))

values <- readxl::read_excel(here::here("tmp","TARA_SAMPLES_CONTEXT_METHODS_20180320.xlsx"),range="B23:BA34798",col_names = FALSE,col_types="text") |>
  data.table()

new_colnames <- c(
  "sample_id_pangaea",
  "sample_id_biosamples",
  "sample_id_ena",
  "station_label",
  "campaign_label",
  "event_label",
  "event_device_label",
  "event_comment",
  "event_date",
  "event_latitude",
  "event_longitude",
  "env_feature",
  "env_feature_extended",
  "depth_nominal",
  "depth_top",
  "depth_bottom",
  "size_fraction_lower_threshold",
  "size_fraction_upper_threshold",
  "sample_material",
  "sample_method",
  "sample_code",
  "sample_description",
  "sample_id_pangaea_replicates",
  "sample_id_pangaea_aliquots",
  "link_event_logsheet",
  "link_wetlab_logsheet",
  "link_drylab_logsheet",
  "link_oceano_context",
  "link_campaign_desc",
  "operator",
  "volume_collected",
  "volume_dilution",
  "volume_processed",
  "conversion_factor",
  "lower_threshold",
  "lower_threshold_device",
  "upper_threshold",
  "upper_threshold_device",
  "sample_container",
  "sample_content",
  "chemical_treatment",
  "temperature_treatment",
  "sample_barcode_stickers",
  "sample_repository",
  "comment_id",
  "comment_event",
  "comment_protocol",
  "comment_fractionation",
  "comment_processing_time",
  "comment_container",
  "comment_chemical_treatment",
  "comment_temperature_treatment"
)

variables[,name:=new_colnames]
setnames(values,new_colnames)

output[["method"]][["values"]] <- values
output[["method"]][["variables"]]  <- variables[,list(name,parameter,pi,method,comment)]

## watercolumn 2

values <- fread(cmd=paste(command_awk,here::here("data","TARA_SAMPLES_CONTEXT_ENV-WATERCOLUMN2.tab")))
variables <- fread(cmd=paste("awk -f",here::here("scripts", "awk","parse_variable.awk"),here::here("data","TARA_SAMPLES_CONTEXT_ENV-WATERCOLUMN2.tab")))

new_colnames <- c(
  "sample_id_pangaea",
  "sample_id_biosamples",
  "sample_id_ena",
  "event_basis",
  "event_compaign",
  "station_label",
  "event_method",
  "event_label",
  "event_time",
  "event_latitude",
  "event_longitude",
  "env_feature",
  "depth_nominal",
  "depth_top",
  "depth_bottom",
  "size_fraction_lower_threshold",
  "size_fraction_upper_threshold",
  "sample_material",
  "sample_method",
  "sample_code",
  "layer_thickness",
  "pelagic_zone",
  "light_env",
  "oxygen_env",
  "nutrient_env",
  "aggreg_photo_org",
  "stratification",
  "proximity_sea_floor",
  "env_feature_other",
  "oxygen_env_2",
  "nutrient_env_2",
  "aggreg_photo_org_2",
  "kd490_amodis",
  "kd490_amodis_8days",
  "kd490_amodis_30days",
  "kd490_globcolour",
  "kd490_globcolour_8days",
  "bbp470_globcolour_8days",
  "acdom_globcolour_8days",
  "kdpar1_amodis_30days_avg",
  "kdpar2_amodis_30days_avg",
  "secchi_disk",
  "depth_euphotic_zone_globcolour_8days",
  "depth_euphotic_zone_secchi_par_amodis_30days_avg",
  "depth_euphotic_zone_kdpar1_par_amodis_30days_avg",
  "depth_euphotic_zone_kdpar2_par_amodis_30days_avg",
  "mixed_layer_depth_sigma",
  "mixed_layer_depth_temperature",
  "depth_chl_max",
  "depth_max_brunt_väisälä",
  "depth_max_oxygen",
  "depth_min_oxygen",
  "depth_nitrocline",
  add_depth("temperature"),
  add_depth("salinity"),
  add_depth("density"),
  add_depth("brunt_väisälä"),
  add_depth("chla"),
  add_depth("oxygen"),
  add_depth("nitrate")
)

variables[,name:=new_colnames]
setnames(values,new_colnames)

output[["watercolumn2"]][["values"]] <- values
output[["watercolumn2"]][["variables"]] <- variables[,list(name,parameter,pi,method,comment)]

# Conversion for numeric columns

for(i in 1:length(output)){
  for(j in names(output[[i]][["values"]])){
    output[[i]][["values"]][,(j):=num_column_conv(get(j))]
  }
}

saveRDS(output,file = here::here("output","Tara_Oceans_Pangaea_context.rds"))

