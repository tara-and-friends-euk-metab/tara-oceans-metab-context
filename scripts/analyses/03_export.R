library(data.table)

# Input
sk <- readLines("metab_samples.txt")
context <- readRDS(here::here("output","Tara_Oceans_Pangaea_context_metaB.rds"))

# General context

## selection

var_selection <- matrix(
  c(
    "method","sample_material",
    "method","event_date",
    "method","event_latitude",
    "method","event_longitude",
    "method","depth_nominal",
    "watercolumn","marine_biome",
    "watercolumn","ocean_region",
    "watercolumn","biogeo_province"
  ),
  ncol=2,byrow = TRUE
) |> data.table() |>  split(by="V1")

tmp <- lapply(var_selection, function(X){
  table_type <- unique(X[,V1])
  table_var <- c("sample_id_pangaea",X[,V2])
  context[[table_type]][["values"]][,.SD,.SDcols=table_var]
})

context_gen_values <- Reduce(function(...) merge(..., all = TRUE), tmp)
context_gen_values <- context_gen_values[sample_id_pangaea%in%sk] 

context_gen_variables <- lapply(var_selection, function(X){
  table_type <- unique(X[,V1])
  data.table(context[[table_type]][["variables"]][name%in%X[,V2]],table=table_type)
}) |> rbindlist()

# add extra columns

context_gen_values[,c("station","depth","size_fraction"):=tstrsplit(sample_material,split="_",keep=c(2,3,4))]
context_gen_values[,c("lower_size_fraction","upper_size_fraction"):=lapply(tstrsplit(size_fraction,split="-"),as.numeric)]
context_gen_values[depth%in%c("SRF","MES"),depthplot:=depth]
context_gen_values[depth%in%c("MIX","DCM"),depthplot:="MIX/DCM"]

x <- context_gen_values[,list(size_fraction,lower_size_fraction)] |>
  unique()

x <- x[order(lower_size_fraction),size_fraction]

context_gen_values[,size_fraction:=factor(size_fraction,levels = x)]
context_gen_values[,depthplot:=factor(depthplot,levels = c("SRF","MIX/DCM","MES"))]
context_gen_values[lower_size_fraction==0.22,sizeplot:="pico"]
context_gen_values[lower_size_fraction==0.8,sizeplot:="piconano"]
context_gen_values[lower_size_fraction%in%c(3,5),sizeplot:="nano"]
context_gen_values[lower_size_fraction==20,sizeplot:="micro"]
context_gen_values[lower_size_fraction==180,sizeplot:="meso"]
context_gen_values[,sizeplot:=factor(sizeplot,levels = c("pico","piconano","nano","micro","meso"))]
context_gen_values[,station_plot:=paste(station,depthplot)]
context_gen_values[,biomeplot:=sub(" .+$","",marine_biome)]
context_gen_values[,polar:=ifelse(biomeplot=="Polar","Polar","Non polar")]
context_gen_values[,abs_lat:=abs(event_latitude)]

prio_size <- data.table(
  size_fraction=c('0.22-1.6','0.22-3','0.8-5','0.8-20','0.8->','0.8-3','3->','3-20','5-20','20-180','180-2000'),
  priority=c(2,1,2,4,1,3,3,1,2,1,1)
)

prio_size <- merge(context_gen_values[,list(sample_id_pangaea,sizeplot,size_fraction,station_plot)],prio_size,by="size_fraction")
prio_size <- split(prio_size, by=c("station_plot", "sizeplot"))
prio_size <- prio_size[lapply(prio_size, nrow)>0]
prio_size <- lapply(prio_size,function(X){
  X[order(priority)][1]
}) |> rbindlist()

context_gen_values[sample_id_pangaea%in%prio_size[,sample_id_pangaea],uniq:="yes"]
context_gen_values[!sample_id_pangaea%in%prio_size[,sample_id_pangaea],uniq:="no"]

x <- context_gen_values[uniq=="yes",length(unique(sizeplot)),by=station_plot][V1==5,station_plot]
context_gen_values[station_plot%in%x&uniq=="yes",complete:="yes"]
context_gen_values[!(station_plot%in%x&uniq=="yes"),complete:="no"]
context_gen_values[station%in%c("049","057","062",as.character(113:121)),coral_station:="yes"]
context_gen_values[!station%in%c("049","057","062",as.character(113:121)),coral_station:="no"]
context_gen_values <- context_gen_values[sample_id_pangaea%in%sk]

saveRDS(context_gen_values,here::here("output","context_general.rds"))
fwrite(context_gen_values,here::here("output","context_general.tsv"),sep="\t",quote=FALSE)
fwrite(context_gen_variables,here::here("output","context_general.info"),sep="\t")

# context for stats
## selection

var_selection <- matrix(
  c(
    "watercolumn","par_day_amodis_30days_avg",
    "watercolumn","depth_bathy",
    "watercolumn","maximum_lyapunov_exponent",
    "sensors","temperature_q2",
    "sensors","chla_npq_cor_calib_q2",
    "sensors","backscattering_coef_470_cor_dark_q2",
    "watercolumn2","depth_chl_max",
    "watercolumn2","mixed_layer_depth_sigma",
    "watercolumn2","depth_max_brunt_väisälä",
    "nut","nitrite_q2",
    "nut","phosphate_q2",
    "nut","nitrate_nitrite_q2",
    "nut","silicate_q2"
  ),
  ncol=2,byrow = TRUE
) |> data.table() |> split(by="V1")

tmp <- lapply(var_selection, function(X){
  table_type <- unique(X[,V1])
  table_var <- c("sample_id_pangaea",X[,V2])
  context[[table_type]][["values"]][,.SD,.SDcols=table_var]
})

context_stat_values <- Reduce(function(...) merge(..., all = TRUE), tmp)
context_stat_values <- context_stat_values[sample_id_pangaea%in%sk]
setnames(context_stat_values,names(context_stat_values),sub("_q2$","",names(context_stat_values)))
setnames(context_stat_values,
         c("par_day_amodis_30days_avg","backscattering_coef_470_cor_dark","maximum_lyapunov_exponent","chla_npq_cor_calib"),
         c("par","backscattering","lyapunov_exp","chla"))

# add extra columns

context_stat_values[,nstar:=nitrate_nitrite-16*phosphate]

context_stat_variables <- lapply(var_selection, function(X){
  table_type <- unique(X[,V1])
  data.table(context[[table_type]][["variables"]][name%in%X[,V2]],table=table_type)
}) |> rbindlist()

saveRDS(context_stat_values,here::here("output","context_stat.rds"))
fwrite(context_stat_values,here::here("output","context_stat.tsv"),sep="\t",quote=FALSE)
fwrite(context_stat_variables,here::here("output","context_stat.info"),sep="\t")
