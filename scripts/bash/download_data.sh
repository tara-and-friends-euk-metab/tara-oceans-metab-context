#!/bin/bash

# Create directories
mkdir -p data tmp output samples

# Empty data folder
rm -f data/*

# Methodology used in the lab for molecular analyses and links to the Sequence
# Read Archive of selected samples from the Tara Oceans Expedition (2009-2013)
# PANGAEA, https://doi.org/10.1594/PANGAEA.875581

wget https://store.pangaea.de/Projects/TARA-OCEANS/Samples_Registry/TARA_SAMPLES_CONTEXT_SEQUENCING_20170515.zip -P data

# Environmental context of all samples from the Tara Oceans Expedition (2009-2013),
# about mesoscale features at the sampling location.
# PANGAEA, https://doi.org/10.1594/PANGAEA.875577,

wget https://doi.pangaea.de/10.1594/PANGAEA.875577?format=textfile -O data/TARA_SAMPLES_CONTEXT_ENV-WATERCOLUMN.tab

# Environmental context of all samples from the Tara Oceans Expedition (2009-2013),
# about carbonate chemistry in the targeted environmental feature.
# PANGAEA, https://doi.org/10.1594/PANGAEA.875567,

wget https://store.pangaea.de/Projects/TARA-OCEANS/Samples_Registry/TARA_SAMPLES_CONTEXT_ENV-DEPTH-CARB_20170515.zip -P data

# Environmental context of all samples from the Tara Oceans Expedition (2009-2013),
# about nutrients in the targeted environmental feature.
# PANGAEA, https://doi.org/10.1594/PANGAEA.875575,

wget https://store.pangaea.de/Projects/TARA-OCEANS/Samples_Registry/TARA_SAMPLES_CONTEXT_ENV-DEPTH-NUT_20170515.zip -P data

# Environmental context of all samples from the Tara Oceans Expedition (2009-2013),
# about sensor data in the targeted environmental feature.
# PANGAEA, https://doi.org/10.1594/PANGAEA.875576,

wget https://doi.pangaea.de/10.1594/PANGAEA.875576?format=textfile -O data/TARA_SAMPLES_CONTEXT-DEPTH-SENSORS.tab

# Environmental context of all samples from the Tara Oceans Expedition (2009-2013),
# about pigment concentrations (HPLC) in the targeted environmental feature.
# PANGAEA, https://doi.org/10.1594/PANGAEA.875569,

wget https://store.pangaea.de/Projects/TARA-OCEANS/Samples_Registry/TARA_SAMPLES_CONTEXT_ENV-DEPTH-HPLC_20170515.zip -P data

# Methodology used on board to prepare samples from the Tara Oceans Expedition (2009-2013).
# PANGAEA, https://doi.org/10.1594/PANGAEA.875580,

wget https://store.pangaea.de/Projects/TARA-OCEANS/Samples_Registry/TARA_SAMPLES_CONTEXT_METHODS.zip -P data

# Environmental context of all samples from the Tara Oceans Expedition (2009-2013),
# about the water column features at the sampling location.
# PANGAEA, https://doi.org/10.1594/PANGAEA.875579,

wget https://doi.pangaea.de/10.1594/PANGAEA.875579?format=textfile -O data/TARA_SAMPLES_CONTEXT_ENV-WATERCOLUMN2.tab
