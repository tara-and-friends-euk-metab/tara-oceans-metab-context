# Extracting contextual data from Pangaea for *Tara* Oceans metabarcoding data sets

## Description

This repository contains several scripts allowing you to download and parse Tara Oceans contextual data from [Pangaea](https://pangaea.de/).

## Download the data

You need first to download the data. To do so, you can run the bash script `scripts/bash/download_data.sh`

## Parse the raw files

The downloaded files (text and xls files) are then parsed and gathered into a list using the script `scripts/analyses/01_pangaea_parser.R`. Each element of the list contains a table `variables`, describing the different parameters from the original file, and a table `values` containing the values associated to the parameters for each sample. The list is saved as a R object. 

## Subset

The output from the previous script is subset to keep only samples from which we have metabarcoding data. This is done using the script `scripts/analyses/02_subset_metaB_samples.R`.

## Export

Finally, contextual data are exported with the script `scripts/analyses/03_export.R`. This script produces two tables. The first one `output/context_general.rds` give a general description of the samples. This is where you will find the station number, the size fraction, the geographic position and the depth for example. The second table, `output/context_stat.rds` contains a series of physico-chemical measurements that can be used for statistical analysis. These two tables are also exported as a text file.